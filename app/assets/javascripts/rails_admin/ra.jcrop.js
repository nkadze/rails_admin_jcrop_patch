(function($) {
  $.widget("ra.jcropForm", {

    _create: function() {
      var widget = this;
      var dom_widget = widget.element;

      var thumbnailLink = dom_widget.find('img.img-polaroid').parent();
      thumbnailLink.unbind().bind("click", function(e){
        widget._bindModalOpening(e, dom_widget.find('a.jcrop_handle').data('link'));
        return false;
      });
      widget._manage_changes();
    },

    _bindModalOpening: function(e, url) {
      e.preventDefault();
      widget = this;
      if($("#modal").length)
        return false;

      var dialog = this._getModal();

      setTimeout(function(){ // fix race condition with modal insertion in the dom (Chrome => Team/add a new fan => #modal not found when it should have). Somehow .on('show') is too early, tried it too.
        $.ajax({
          url: url,
          beforeSend: function(xhr) {
            xhr.setRequestHeader("Accept", "text/javascript");
          },
          success: function(data, status, xhr) {
              dialog.find('.modal-body').html(data);
              widget._bindFormEvents();
          },
          error: function(xhr, status, error) {
            dialog.find('.modal-body').html(xhr.responseText);
          },
          dataType: 'text'
        });
      },100);

    },

    _bindFormEvents: function() {
      var widget = this,
          dialog = this._getModal(),
          form = dialog.find("form"),
          saveButtonText = dialog.find(":submit[name=_save]").html(),
          cancelButtonText = dialog.find(":submit[name=_continue]").html();
      dialog.find('.form-actions').remove();	

      var jcrop_options = $.extend({
        bgColor: 'white',
        keySupport: false,
        onSelect: widget.updateCoordinates,
        onChange: widget.alertJcropOptimalSize
      }, widget.getCurrentOptionField());

      var jcrop_subject = dialog.find('img.jcrop-subject');
      jcrop_subject.Jcrop(jcrop_options);
      
      var geometry_data = $.map(jcrop_subject.attr('data-geometry').split(','), function(value){
          return parseInt(value, 10);
      });

      window['rails_admin_jcrop_geometry'] = geometry_data;


      form.attr("data-remote", true);
      dialog.find('.modal-header-title').text(form.data('title'));
      dialog.find('.cancel-action').unbind().click(function(){
        dialog.modal('hide');
        return false;
      }).html(cancelButtonText);

      dialog.find('.save-action').unbind().click(function(){
        form.submit();
        return false;
      }).html(saveButtonText);

      $(document).trigger('rails_admin.dom_ready');

      form.bind("ajax:complete", function(xhr, data, status) {
        if (status == 'error') {
          dialog.find('.modal-body').html(data.responseText);
          widget._bindFormEvents();
        } else {
          var json = $.parseJSON(data.responseText);
          var select = widget.element.find('select').filter(":hidden");

          thumb = widget.element.find('a.jcrop_handle').data('thumb');
          widget.element.find('img.img-polaroid').removeAttr('src').attr('src', json.urls[thumb] + '?' + new Date().valueOf());

          widget._trigger("success");
          dialog.modal("hide");
        }
      });
    },

    updateCoordinates: function(c) {
      var rx = 100/c.w;
      var ry = 100/c.h;
      var lw = $('img.jcrop-subject').width();
      var lh = $('img.jcrop-subject').height();
      var ratio = $('img.jcrop-subject').data('geometry').split(',')[0] / lw ;

      $('#preview').css({
        width: Math.round(rx * lw) + 'px',
        height: Math.round(ry * lh) + 'px',
        marginLeft: '-' + Math.round(rx * c.x) + 'px',
        marginTop: '-' + Math.round(ry * c.y) + 'px'
      });

      $("#crop_x").val(Math.round(c.x * ratio));
      $("#crop_y").val(Math.round(c.y * ratio));
      $("#crop_w").val(Math.round(c.w * ratio));
      $("#crop_h").val(Math.round(c.h * ratio));
    },

    _getModal: function() {
      var widget = this;
      if (!widget.dialog) {
          var currentOptionFieldName = widget.getCurrentOptionFieldName(); 
          widget.dialog = $('<div id="modal" class="modal fade" data-option-field="' + currentOptionFieldName + '"><div class="modal-header"><a href="#" class="close" data-dismiss="modal">&times;</a><h3 class="modal-header-title">...</h3></div><div class="modal-body">...</div><div class="modal-footer"><a href="#" class="btn cancel-action">...</a><a href="#" class="btn btn-primary save-action">...</a></div></div>');
          widget.dialog.modal({
            keyboard: true,
            backdrop: true,
            show: true
          })
          .on('hidden', function(){
            widget.dialog.remove();   // We don't want to reuse closed modals
            widget.dialog = null;
          })
          .on('shown.bs.modal', function (){
              $('#modal').find('img.jcrop-subject').one("load", function() {
                 $('#modal').find('.modal-body').css('max-height', $('#modal').find('form').height() + 20);
              })
              .each(function() {
                if(this.complete) 
                  $(this).load();
              });
              $('#modal').find('.modal-body').one('click', function() {
                 $('#modal').find('.modal-body').css('max-height', $('#modal').find('form').height() + 20);
              });    
          });
        }
      return this.dialog;
    },

    alertJcropOptimalSize: function(c){   
      var modal = $('#modal');
      var imageContianer = modal.find('.jcrop-holder').first().find('.jcrop-tracker').first();     
      var actualImage = modal.find('.jcrop-tracker').last();
      var optionFieldName = modal.attr('data-option-field');      
      var optionFiled = window[optionFieldName];
      if (optionFiled.recomendedSize) {
        var geometry_data = window['rails_admin_jcrop_geometry'];
        if ((Math.abs(c.x2 - c.x) < optionFiled.recomendedSize[0] * (actualImage.width()/geometry_data[0])) || 
            (Math.abs(c.y2 - c.y) < optionFiled.recomendedSize[1] * (actualImage.height()/geometry_data[1]))){
            imageContianer.css('background-color','rgba(255, 143, 16, 0.18)');
            modal.find('.rails_admin_jcrop_hint').remove();
            modal.find('.modal-footer').prepend('<p class="rails_admin_jcrop_hint" style="color: #1AA017;">Small</p><p class="rails_admin_jcrop_hint" style="color: #AD2B2B;">Medium</p><p class="rails_admin_jcrop_hint" style="color: #AD2B2B;">Large</p>');
        }
        else if (optionFiled.mediumSize &&
                 ((Math.abs(c.x2 - c.x) < optionFiled.mediumSize[0] * (actualImage.width()/geometry_data[0])) || 
                  (Math.abs(c.y2 - c.y) < optionFiled.mediumSize[1] * (actualImage.height()/geometry_data[1])))) {
          imageContianer.css('background-color','rgba(140, 255, 16, 0.180392)');          
            modal.find('.rails_admin_jcrop_hint').remove();
            modal.find('.modal-footer').prepend('<p class="rails_admin_jcrop_hint" style="color: #1AA017;">Small</p><p class="rails_admin_jcrop_hint" style="color: #1AA017;">Medium</p><p class="rails_admin_jcrop_hint" style="color: #AD2B2B;">Large</p>');
        }
        else{
          imageContianer.css('background-color','transparent');  
          modal.find('.rails_admin_jcrop_hint').remove();
          modal.find('.modal-footer').prepend('<p class="rails_admin_jcrop_hint" style="color: #1AA017;">Small</p><p class="rails_admin_jcrop_hint" style="color: #1AA017;">Medium</p><p class="rails_admin_jcrop_hint" style="color: #1AA017;">Large</p>');
        }
      }
    },

    getCurrentOptionField: function(){
        var widget = this;
        var currentOptionFieldName = widget.getCurrentOptionFieldName();
        return window[currentOptionFieldName];
    },
    
    getCurrentOptionFieldName: function(){
       var widget = this;
       var jcrop_current_field_name = $(widget.element).find('div[id^="jcrop_script_"]').first().attr('id').replace('jcrop_script_', ''); 
       return "rails_admin_jcrop_options_" + jcrop_current_field_name;
    },

    destroy: function(){
        var widget = this;
        var dom_widget = widget.element;
        var thumbnailLink = dom_widget.find('img.img-polaroid').parent();
        thumbnailLink.unbind();
    },

    _manage_changes: function(){
      var widget = this;
      var dom_widget = widget.element;
      dom_widget.find('.jcrop_handle').each(function (){     
            var that = $(this);
            var toggle = that.parent();
            toggle.bind("DOMSubtreeModified",function(){
              var preview = toggle.find('.preview');
              if (preview.length && preview.attr('src') && preview.attr('src').match("^data")) {            
                  var image_link = preview.next('a').first();
                  var cached_preview = preview;
                  image_link.attr('href', preview.attr('src'));
                  preview.remove();
                  image_link.find('img').remove();
                  cached_preview.attr('class', 'img-polaroid');
                  cached_preview.attr('style', 'width: 100px;');
                  image_link.html(cached_preview);
                  var form = $('<form></form>').attr('action', '/admin/photos/save_temp_image').attr('method', 'post').attr('enctype', 'multipart/form-data');
                  var file = toggle.find('input[type="file"]');
                  var cached_file = file;
                  form.append(cached_file);
                  var input_auth = $('<input name="authenticity_token" type="hidden"/>').val($('input[name="authenticity_token"]').val());
                  form.append(input_auth);
                  form.append('<input name="utf8" type="hidden" value="✓"/>');

                  var object_name = window.location.pathname.replace('/admin/', '').split('/')[0];
                  var object_id = window.location.pathname.replace('/admin/', '').split('/')[1];

                  form.append('<input name="object_id" type="hidden" value="' + object_id + '"/>');
                  form.append('<input name="object_name" type="hidden" value="' + object_name + '"/>');

                  var matches = cached_file.attr('name').match(/\[(.*?)\]/);
                  if (matches) {
                      var submatch = matches[1];
                      form.append('<input name="field_name" type="hidden" value="' + submatch + '"/>');
                  }

                  form.hide();
                  $('form').after(form);

                  form.submit(function(e) {                
                      $.ajax({
                        url: form.attr('action'),
                        type: 'POST',
                        data: new FormData( this ),
                        processData: false,
                        contentType: false,
                        success: function (res){                          
                          var jcrop_link = toggle.find('.jcrop_handle');
                          jcrop_link.before(cached_file);
                          jcrop_link.attr('data-link', removeParam('temp_image', jcrop_link.attr('data-link')));
                          jcrop_link.attr('data-link', jcrop_link.attr('data-link') + '&temp_image=' + res);

                          var thumbnailLink = dom_widget.find('img.img-polaroid').parent();
                              thumbnailLink.unbind().bind("click", function(e){
                                  widget._bindModalOpening(e, jcrop_link.attr('data-link'));
                                  return false;
                          });
                          form.remove();
                        }
                      });
                      e.preventDefault();
                  });
                  form.submit();
              }
            });
        });
    }
  });
})(jQuery);

$(document).on('rails_admin.dom_ready', function(){ 
  $('div.jcrop_type').jcropForm();
 });

function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}